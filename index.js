const DUPLICATE_ID = Symbol("Duplicate ID"),
      DUPLICATE_ROUTE = Symbol("Canonical route defined twice"),
      INVALID_SPEC = Symbol("Invalid spec"),
      MISSING_ID = Symbol("Missing ID property"),
      NOT_ENOUGH_URL_PARAMS = Symbol("Not enough URL params"),
      ERROR = Symbol("Whither handler error"),
      HANDLER = Symbol("Whither handler"),
      ROUTE = Symbol("Matched route");

function error (message, properties) {
    return Object.assign(new Error(message), properties);
}

function deepAssign (map, [key, ...keys], value) {
    if (keys.length) {
        map[key] = map[key] || {};
        deepAssign(map[key], keys, value);
    } else {
        map[key] = value;
    }
    return map;
}

function deepGet (map, [key, ...keys]) {
    if (keys.length && map[key]) {
        return deepGet(map[key], keys);
    }
    return map[key];
}

function compile (spec) {
    if (spec !== Object(spec) || Array.isArray(spec)) {
        throw error("Compile expects a plain object with URL paths as keys and objects as values.", {
            "type": INVALID_SPEC,
        });
    }
    const lookup = {},
          reverse = {};

    for (const [path, methods] of Object.entries(spec).sort()
        .reverse()) {
        if (methods !== Object(methods) || Array.isArray(methods)) {
            throw error(`The key [${path}] must have an object as value.`, {
                "type": INVALID_SPEC,
            });
        }
        const parts = path.split("/").filter(Boolean);
        for (const [method, map] of Object.entries(methods)) {
            if (map !== Object(map) || Array.isArray(map)) {
                throw error(`The key [${path}][${method}] must have an object as value.`, {
                    "type": INVALID_SPEC,
                });
            }
            if (map.id === undefined) {
                throw error(`Route "${method} ${path}" is missing required key "id".`, {
                    "type": MISSING_ID,
                });
            }
            if (reverse[map.id]) {
                const { [map.id]: conflict } = reverse;
                throw error(`Route ID "${map.id}" used twice for path "${method} ${path}" and "${conflict[0]} ${conflict[1].urlTemplate}".`, {
                    "type": DUPLICATE_ID,
                });
            }
            if (deepGet(lookup, parts.concat(method))) {
                throw error(`Canonical route "${method} ${parts.join("/")}" is defined multiple times. Check route IDs "${deepGet(lookup, parts.concat(method)).id}" and "${map.id}".`, {
                    "type": DUPLICATE_ROUTE,
                });
            }
            const { "length": wildcardCount } = parts.filter(s => s === "*"),
                  enrichedMap = {
                      method,
                      "urlTemplate": path,
                      wildcardCount,
                      ...map,
                  };
            deepAssign(lookup, parts.concat(method), enrichedMap);
            reverse[map.id] = enrichedMap;
        }
    }

    if (Object.keys(reverse).length === 0) {
        throw error("Your spec resulted in an empty Router. Did you remember all three layers: paths, methods, and endpoints? E.g. { '/users': { 'GET': { 'id': 'getAllUsers' } } }?", {
            "type": INVALID_SPEC,
        });
    }

    return { lookup, reverse };
}

function byUrl (routes, method, url) {
    const parts = url.split("/").filter(Boolean);
    function rec (routesPart, urlParts) {
        const [part, ...rest] = urlParts;
        if (!part) {
            return routesPart[method] || routesPart["*"];
        }

        return (routesPart[part] && rec(routesPart[part], rest))
            || (routesPart["*"] && rec(routesPart["*"], rest))
            || (routesPart["**"] && rec(routesPart["**"], []));
    }
    const match = rec(routes.lookup, parts);
    return match && { url, ...match };
}

function byName (routes, id, ...routeArgs) {
    const { "reverse": { [id]: map } } = routes;
    if (!map) {
        return undefined;
    }
    if (routeArgs.length < map.wildcardCount) {
        throw error(`Route '${id}' has template '${map.urlTemplate}', which has ${map.wildcardCount} wildcard URL parts, but you supplied ${routeArgs.length}.`, NOT_ENOUGH_URL_PARAMS, {
            "route": { "url": null, ...map },
            "type": NOT_ENOUGH_URL_PARAMS,
        });
    }
    const parts = map.urlTemplate.split("/");
    let part = 0;
    for (let i = 0; i < parts.length; i++) {
        if (parts[i] === "*") {
            parts[i] = routeArgs[part++];
        }
    }
    return { "url": parts.join("/"), ...map };
}

function handler (routes) {
    return request => new Promise(resolve => {
        const { "pathname": path } = new URL(request.url, "a://a"),
              route = byUrl(routes, request.method, path);
        if (!route) {
            return resolve({ "status": 404 });
        }
        if (!route[HANDLER]) {
            return resolve({ [ROUTE]: route, "status": 404 });
        }
        try {
            const response = route[HANDLER](request);
            if (response.then) {
                return response
                    .then(resolved => resolve({ [ROUTE]: route, ...resolved }))
                    .catch(handlerError => resolve({
                        [ERROR]: handlerError,
                        [ROUTE]: route,
                        "status": 500,
                    }));
            }
            return resolve({ [ROUTE]: route, ...response });
        } catch (handlerError) {
            return resolve({
                [ERROR]: handlerError,
                [ROUTE]: route,
                "status": 500,
            });
        }
    });
}

module.exports = {
    DUPLICATE_ID,
    DUPLICATE_ROUTE,
    ERROR,
    HANDLER,
    MISSING_ID,
    NOT_ENOUGH_URL_PARAMS,
    ROUTE,
    byName,
    byUrl,
    compile,
    handler,
};
