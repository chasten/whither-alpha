# `whither-alpha`

> Part of [Chasten](https://chasten.gitlab.io).

`whither-alpha` is currently in alpha and makes no promises about API stability. It will eventually be superceded by `whither` (without the alpha), which will be a new project, a new NPMJS package, and a new git repository.

## Installing / Getting started

To use `whither-alpha` you'll need the latest [Node.js](https://nodejs.org) LTS version or newer and a Node.js package manager of your choosing. The examples all assume `npm`, but the alternatives work just as well.

Here's a snippet you can copy/paste into Bash to get a working example. You can safely run this in any empty directory.

```shell
npm init -y
npm install @chasten/whither-alpha

cat <<'EOF' >> example.js
const http = require('http');
const { requestListener } = require('@chasten/strait-alpha');
const handler = request => ({
  "body": JSON.stringify(request),
  "headers": { "content-type": "application/json" },
  "status": 200,
});
http.createServer(requestListener(handler)).listen(3000);
EOF

node example.js&
curl http://localhost:3000
```

You'll now have a new file called example.js, you'll have a running node process listening on port 3000, and you'll have sent a cURL request to see what your web service responds.

## Developing

If you'd like to develop `whither-alpha` on your local machine you can get started pretty easily:

```shell
git clone https://gitlab.com/chasten/whither-alpha.git
cd whither-alpha/
npm ci
```

This will clone the repository from gitlab, change directory, and install the required development dependencies.

### Deploying / Publishing

In order to publish new versions of `whither-alpha` you'll need public permissions for the organization on NPMJS and you'll need to be logged in with `npm` when you run the following command:

```shell
npm publish
```

This publishes a version (specified in the `version` field in `package.json`) to NPMJS.com.

## Features

`whither-alpha` may work stand-alone as a routing library or in conjunction with `strait-alpha` to bind together routing with request handling. More to come.

## Contributing

`whither-alpha` happily accepts contributions. Contributing to the project has a few requirements:

-   The rules outlined in CONTRIBUTING.md must be followed.
-   The ESLint style guide (@chasten/eslint-config) must be followed.

The latter should be set up in your editor/IDE of choice

## Links

- Project homepage: https://chasten.gitlab.io
- Repository: https://gitlab.com/chasten/whither-alpha
- Issue tracker: https://gitlab.com/chasten/whither-alpha/issues
  - In case of sensitive bugs like security vulnerabilities, please create a [confidential issue](https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html) on GitLab. We value your effort
    to improve the security and privacy of this project!
- Related projects:
  - [`strait-alpha`](https://gitlab.com/chasten/strait-alpha)

## Licensing

The code in this project is licensed under MIT license.
