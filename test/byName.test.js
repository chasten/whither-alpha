const { byName, compile } = require("../index");

it("returns an endpoint given its ID", () => {
    const router = compile({
        "/users": {
            "GET": {
                "id": "getAllUsers",
                "metaData": "123",
            },
        },
    });
    expect(byName(router, "getAllUsers")).toMatchObject({
        "metaData": "123",
    });
});

it("returns undefined for unknown IDs", () => {
    const router = compile({
        "/users": {
            "GET": {
                "id": "getAllUsers",
                "metaData": "123",
            },
        },
    });
    expect(byName(router, "wrong id")).toBeUndefined();
});

it("fills in the given URL wildcards", () => {
    const router = compile({
        "/users/*/pets/*": {
            "GET": {
                "id": "getUserPet",
            },
        },
    });
    expect(byName(router, "getUserPet", "1234", "9876")).toMatchObject({
        "url": "/users/1234/pets/9876",
    });
});

it("throws an error if missing required URL wildcards", () => {
    const router = compile({
        "/users/*/pets/*": {
            "GET": {
                "id": "getUserPet",
            },
        },
    });
    expect(() => byName(router, "getUserPet")).toThrow();
});
