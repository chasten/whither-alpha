const { byUrl, compile } = require("../index");

it("returns undefined for unknown paths", () => {
    const router = compile({
        "/users/*/pets/*": {
            "GET": {
                "id": "getUserPet",
            },
        },
    });
    expect(byUrl(router, "GET", "/wrong/url")).toBeUndefined();
});

it("returns undefined for unknown methods", () => {
    const router = compile({
        "/users/*/pets/*": {
            "GET": {
                "id": "getUserPet",
            },
        },
    });
    expect(byUrl(router, "POST", "/users/1234/pets/9876")).toBeUndefined();
});

it("returns the endpoint for unknown path and method", () => {
    const router = compile({
        "/users/*/pets/*": {
            "GET": {
                "id": "getUserPet",
            },
        },
    });
    expect(byUrl(router, "GET", "/users/1234/pets/9876")).toMatchObject({
        "id": "getUserPet",
    });
});

it("prefers a literal match to a single wildcard", () => {
    const router = compile({
        "/*": {
            "GET": {
                "id": "wildcard",
            },
        },
        "/users": {
            "GET": {
                "id": "literal",
            },
        },
    });
    expect(byUrl(router, "GET", "/users")).toMatchObject({
        "id": "literal",
    });
});

it("prefers a literal match to a double wildcard", () => {
    const router = compile({
        "/**": {
            "GET": {
                "id": "wildcard",
            },
        },
        "/users": {
            "GET": {
                "id": "literal",
            },
        },
    });
    expect(byUrl(router, "GET", "/users")).toMatchObject({
        "id": "literal",
    });
});

it("prefers a single wildcard to a double wildcard", () => {
    const router = compile({
        "/*": {
            "GET": {
                "id": "single wildcard",
            },
        },
        "/**": {
            "GET": {
                "id": "double wildcard",
            },
        },
    });
    expect(byUrl(router, "GET", "/users")).toMatchObject({
        "id": "single wildcard",
    });
});

it("backtracks to an earlier single wildcard", () => {
    const router = compile({
        "*/*/houses": {
            "GET": {
                "id": "wildcard",
            },
        },
        "/users/*/pets": {
            "GET": {
                "id": "literal",
            },
        },
    });
    expect(byUrl(router, "GET", "/users/1234/houses")).toMatchObject({
        "id": "wildcard",
    });
});

it("backtracks to an earlier double wildcard", () => {
    const router = compile({
        "/**": {
            "GET": {
                "id": "wildcard",
            },
        },
        "/users/*/pets": {
            "GET": {
                "id": "literal",
            },
        },
    });
    expect(byUrl(router, "GET", "/users/1234/houses")).toMatchObject({
        "id": "wildcard",
    });
});

it("accepts a single method wildcard", () => {
    const router = compile({
        "/**": {
            "*": {
                "id": "wildcard",
            },
        },
    });
    expect(byUrl(router, "GET", "/users")).toMatchObject({
        "id": "wildcard",
    });
});
