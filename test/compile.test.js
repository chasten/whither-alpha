const { compile } = require("../index");

it("throws an error for missing route ID", () => {
    const spec = {
        "/users/*": {
            "GET": {},
        },
    };
    expect(() => compile(spec)).toThrow();
});

it("throws an error for duplicate route IDs", () => {
    const spec = {
        "/users/*": {
            "GET": {
                "id": "duplicate ID",
            },
            "POST": {
                "id": "duplicate ID",
            },
        },
    };
    expect(() => compile(spec)).toThrow();
});

it("throws an error when canonically equal routes are defined multiple times", () => {
    const spec = {
        "/users/*": {
            "GET": {
                "id": "without trailing slash",
            },
        },
        "/users/*/": {
            "GET": {
                "id": "with trailing slash",
            },
        },
    };
    expect(() => compile(spec)).toThrow();
});

it("throws an error when the spec is invalid", () => {
    const spec = [{}];
    expect(() => compile(spec)).toThrow();
});

it("throws an error when the methods are invalid", () => {
    const spec = {
        "/correct/path": [{}],
    };
    expect(() => compile(spec)).toThrow();
});

it("throws an error when the endpoint is not an object", () => {
    const spec = {
        "/correct/path": {
            "GET": 5,
        },
    };
    expect(() => compile(spec)).toThrow();
});

it("throws an error when the resulting Router is empty", () => {
    const spec = {};
    expect(() => compile(spec)).toThrow();
});

it("compiles a valid spec", () => {
    const spec = {
        "/users": {
            "GET": {
                "id": "getAllUsers",
            },
        },
    };
    expect(() => compile(spec)).not.toThrow();
});
