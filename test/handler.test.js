const { ERROR, HANDLER, ROUTE, compile, handler } = require("../index");

it("should use a matched endpoint's sync handler", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => ({ "status": 200 }),
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({ "status": 200 });
});

it("should use a matched endpoint's async handler", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => Promise.resolve({ "status": 200 }),
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({ "status": 200 });
});

it("should return 404 when no matching endpoint is found", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => ({ "status": 200 }),
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "POST", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({ "status": 404 });
});

it("should return 404 when matched endpoint has no handler", () => {
    const spec = {
        "/path": {
            "GET": {
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({
        "status": 404,
    });
});

it("should return ROUTE when matched endpoint has no handler", () => {
    const spec = {
        "/path": {
            "GET": {
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request).then(res => res[ROUTE])).resolves.toEqual({
        "id": "endpoint",
        "method": "GET",
        "url": "/path",
        "urlTemplate": "/path",
        "wildcardCount": 0,
    });
});

it("should return 500 when matched endpoint throws sync error", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => {
                    throw new Error();
                },
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({
        "status": 500,
    });
});

it("should return ROUTE when matched endpoint throws sync error", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => {
                    throw new Error();
                },
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request).then(res => res[ROUTE])).resolves.toMatchObject({
        "id": "endpoint",
        "method": "GET",
        "url": "/path",
        "urlTemplate": "/path",
        "wildcardCount": 0,
    });
});

it("should return ERROR when matched endpoint throws sync error", () => {
    const error = new Error("what a terrible handler"),
          spec = {
              "/path": {
                  "GET": {
                      [HANDLER]: () => {
                          throw error;
                      },
                      "id": "endpoint",
                  },
              },
          },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request).then(res => res[ERROR])).resolves.toEqual(error);
});

it("should return 500 when matched endpoint throws async error", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => Promise.reject(new Error()),
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request)).resolves.toMatchObject({
        "status": 500,
    });
});

it("should return ROUTE when matched endpoint throws async error", () => {
    const spec = {
        "/path": {
            "GET": {
                [HANDLER]: () => Promise.reject(new Error()),
                "id": "endpoint",
            },
        },
    },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request).then(res => res[ROUTE])).resolves.toMatchObject({
        "id": "endpoint",
        "method": "GET",
        "url": "/path",
        "urlTemplate": "/path",
        "wildcardCount": 0,
    });
});

it("should return ERROR when matched endpoint throws async error", () => {
    const error = new Error("what a terrible handler"),
          spec = {
              "/path": {
                  "GET": {
                      [HANDLER]: () => Promise.reject(error),
                      "id": "endpoint",
                  },
              },
          },
          router = compile(spec),
          h = handler(router),
          request = { "method": "GET", "url": "/path" };
    expect(h(request).then(res => res[ERROR])).resolves.toEqual(error);
});
